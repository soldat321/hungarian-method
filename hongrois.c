/*
Copyright 2017 Abdellaziz Mohamed Amine

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "hongrois.h"

//Procédure de lecture de la matrice des coûts 
//à partir du fichier
void lecture(FILE *fichier,int n, int p, int q, double cost[n][n])
{
    for(int i=0; i<p; i++)
	for(int j=0; j<q; j++)
	    fscanf(fichier,"%lf", &cost[i][j]);
}

//Procédure qui trouve et soustrait la
//valeur minimum dans chaque lignes puis colonnes
void min_matrix(int n, double cost[n][n])
{
    for(int i=0; i<n; i++)
    {
	double min = cost[i][0];
	for(int j=1; j<n; j++)
	    if(cost[i][j] < min)
		min = cost[i][j];
	for(int j=0; j<n; j++)
	    cost[i][j] = cost[i][j] - min;
    }

    for(int j=0; j<n; j++)
    {
	double min = cost[0][j];
	for(int i=1; i<n; i++)
	    if(cost[i][j] < min)
		min = cost[i][j];
	for(int i=0; i<n; i++)
	    cost[i][j] -= min;
    }
}

//Procédure d'initialisation d'une matrice
void initialize_matrix(int n, int matrix[n][n])
{

    for(int i=0; i<n; i++)
	for(int j=0; j<n; j++)
	    matrix[i][j] = 0;
}

//Procédure d'initialisation d'un vecteur
void initialize_vector(int n, int vector[n])
{
    for(int i=0; i<n; i++)
	vector[i] = 0;
}

//Procédure qui copie le contenu de matrix1 dans matrix2
void copy_matrix(int n, double matrix1[n][n], double matrix2[n][n])
{
    for(int i=0; i<n; i++)
	for(int j=0; j<n; j++)
	    matrix2[i][j] = matrix1[i][j];
}

//Affichage de la solution
void affiche(int n, int matrix[n][n], double cost[n][n], char *instance, FILE *fichier)
{

    fprintf(fichier, "\n\n========================================================================");
    setlocale(LC_TIME,"fr_FR.utf8");
    char buff[70];
    time_t date = time(NULL);
    strftime(buff, sizeof buff, "%A %d %B %Y %H:%M:%S", gmtime(&date));
    fprintf(fichier, "\n\n%s", buff);
    fprintf(fichier, "\nInstance: %s", instance);
    fprintf(fichier, "\nMatrice d'affectation (solution):");
    for(int i=0; i<n; i++)
    {
	fprintf(fichier,"\n\t");
	for(int j=0; j<n; j++)
	    fprintf(fichier,"%d  ", matrix[i][j]);
    }
    fprintf(fichier,"\n\n");

    double fo = 0;
    for(int i=0; i<n; i++)
	for(int j=0; j<n;j++)
	    if(matrix[i][j])
		fo += cost[i][j];
    fprintf(fichier,"Le coût minimum d'affectation est: %.2lf\n", fo);
}


void affiche_double(int n, double matrix[n][n], FILE *fichier)
{
    for(int i=0; i<n; i++)
    {
	fprintf(fichier,"\n\t");
	for(int j=0; j<n; j++)
	    fprintf(fichier,"%.2lf  ", matrix[i][j]);
    }
    fprintf(fichier,"\n");
}

int barre_carre(int n, int assignement[n][n], int intermediate[n][n])
{
    int nb_total;
    int nb_assignement = 0;
    do
    {
	int trouve = 0;
	int nb = 0;
	do
	{
	    nb_total = 0;
	    nb++;
	    int i = 0;
	    while(i<n && !trouve)
	    {
		int nb_zeros = 0;
		for(int j=0; j<n; j++)
		    if(intermediate[i][j] == 1)
			nb_zeros++;

		if(nb_zeros == nb)
		{
		    trouve = 1;
		    nb_assignement++;
		    int j = 0;
		    while(intermediate[i][j] != 1)
			j++;
		    assignement[i][j] = 1;
		    intermediate[i][j] = 2;

		    for(int k=0; k<n; k++)
		    {
			if(intermediate[i][k]==1 && k!=j)
			    intermediate[i][k] = 3;
			if(intermediate[k][j]==1 && k!=i)
			    intermediate[k][j] = 3;
		    }
		}
		nb_total += nb_zeros;
		i++;
	    }
	    if(!nb_total)
		break;
	}while(!trouve);
    }while(nb_total);
    if(nb_assignement == n)
	return 1;
    else return 0;
}

//Procédure qui fait correspondre les zéros de la matrcie cost
//à des 1 dans la matrice intermediate
void cost_zeros(int n,double  cost[n][n],int intermediate[n][n])
{
    for(int i = 0; i<n; i++)
	for(int j=0; j<n; j++)
	    if(cost[i][j] == 0)
		intermediate[i][j] = 1;
}


//Procédure de marquage des lignes et colonnes 
void marquer(int n, int intermediate[n][n], int lignes[n], int colonnes[n])
{
    //Marquer les lignes ne contenant aucun zéro barré
    for(int i=0; i<n; i++)
    {
	int aucun = 1;
	for(int j=0; j<n; j++)
	{
	    if(intermediate[i][j] == 2)
		aucun =0;
	}
	if(aucun)
	    lignes[i] = 1;
    }

    //Booléen qui vaut 1 s'il aucune nouvelle ligne ou colonne n'a été marquée, 0 sinon
    int aucun;
    do
    {
	aucun = 1;
	//On marque les colonnes
	for(int j=0; j<n; j++)
	{
	    //On vérifie que la colonne n'est pas déjà marquée
	    if(!colonnes[j])
	    {
		for(int i=0; i<n; i++)
		    //On barre si la colonne contient un zéro appartenant à une ligne marquée
		    if(lignes[i] && intermediate[i][j])
		    {
			colonnes[j] = 1;
			aucun = 0;
		    }
	    }
	}

	//Si au moins une nouvelle colonne a été marquée
	//On procède au marquage des lignes
	if(!aucun)
	{
	    //On marque les lignes
	    for(int i=0; i<n; i++)
	    {
		//On vérifie que la ligne n'est pas déjà marquée
		if(!lignes[i])
		{
		    for(int j =0; j<n; j++)
			//On marque si la ligne contient un zéro carré appartenant à une colonne marquée
			if(colonnes[j] && intermediate[i][j] == 2)
			{
			    lignes[i] = 1;
			    aucun = 0;
			}
		}
	    }
	}
    }while(!aucun);
}


//Procédure qui barre la matrice des couts
void barrer(int n, int  matrice_barre[n][n], int lignes[n], int colonnes[n])
{
    //On barre les lignes
    for(int i=0; i<n; i++)
	if(!lignes[i])
	    for(int j=0; j<n; j++)
		matrice_barre[i][j]++;
    //On barre les colonnes
    for(int j=0; j<n; j++)
	if(colonnes[j])
	    for(int i=0; i<n; i++)
		matrice_barre[i][j]++;
}

//Procédure qui soustrait et additionne la valeur minimum de la matrice des coûts
//selon l'état des la matrice barrée
void soustraction_addition(int n, double cost[n][n], int matrice_barre[n][n])
{
    double min = 0;
    //Initialisation de la valeur minimum
    int i = 0, j = 0;
    while(!min && i<n && j<n) 
    {
	if(!matrice_barre[i][j])
	    min = cost[i][j];
	i++;
	j++;
    }

    //Recherche de la valeur minimum parmi les éléments non barrés
    for(int i=0; i<n; i++)
	for(int j=0; j<n; j++)
	    if(!matrice_barre[i][j] && cost[i][j] < min)
		min = cost[i][j];

    //Les opérations sur les éléments barrés
    for(int i=0; i<n; i++)
	for(int j=0; j<n; j++)
	{
	    //On soustrait le minimum aux éléments barrés une seule fois 
	    if(!matrice_barre[i][j])
		cost[i][j] -= min;
	    //On additionne le minimum aux éléments barrés deux fois 
	    else if(matrice_barre[i][j] == 2)
		cost[i][j] += min;
	}
}
