/*
Copyright 2017 Abdellaziz Mohamed Amine

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "hongrois.h"

int main(int argc, char* argv[])
{
    /*Le programme a besoin de trois arguments en entrée:
     * 1. le nom du fichier
     * 2. le nombre de taches ou de lignes
     * 3. le nombre d'ouvriers ou de colognes
     */

    if(argc < 4)
    {
	printf("Pas assez d'arguments. Usage:\n");
	printf(argv[0]);
	printf(" fichier_instance nb_lignes nb_colonnes\n");
	return 1;
    }
    else
    {
	FILE* fichier;
	fichier = fopen(argv[1],"r" );
	//Test de réussite de l'ouverture du ficher
	if(!fichier)
	{
	    perror("Fichier non existant");
	    return EXIT_FAILURE;
	}

	//Dimensions de la matrice des couts
	int n, p, q;
	sscanf(argv[2],"%d", &p);
	sscanf(argv[3],"%d", &q);

	if(p<q)
	    n = q;
	else
	    n = p;

	//Matrice des couts
	double cost[n][n];
	double cost_copie[n][n];
	/*Matrice des affectations; la solution
	 *assignement[i][j] vaut:
	 *1 si l'ouvrier j est affecté à la tâche i
	 *0 sinon
	 */
	int assignement[n][n];  

	lecture(fichier, n, p, q, cost);
	copy_matrix(n, cost, cost_copie);
	fclose(fichier);


	affiche_double(n, cost, stdout);

	//Booléen qui sauvegarde la condition d'arrêt
	int arret = 0;
	do{
	    min_matrix(n, cost);

	    /*Matrice contenant l'état des zéros de cost[n][n]
	     * Si intermediate[i][j] prend la valeur:
	     * 0 : cost[i][j] est différent de zéro
	     * 1 : cost[i][j] est un zéro (pas encore carré ou barré)
	     * 2 : cost[i][j] est un zéro carré
	     * 3 : cost[i][j] est un zéro barré
	     */
	    int intermediate[n][n];
	    initialize_matrix(n, intermediate);

	    cost_zeros(n, cost, intermediate);

	    initialize_matrix(n, assignement);
	    arret = barre_carre(n, assignement, intermediate);
	    if(!arret)
	    {
		/*vecteurs des lignes et colonnes marquées; les valeurs
		 *1 correspondent à marquée
		 *0 à non marquée
		 */
		int colonnes[n];
		int lignes[n];

		initialize_vector(n, colonnes);
		initialize_vector(n, lignes);

		marquer(n, intermediate, lignes, colonnes);

		/*Matrice qui donne l'état de la matrice des coûts barré;
		 *L'élément matrice_barre[i][j] vaut:
		 *0 si cost[i][j] n'est pas barré
		 *1 si cost[i][j] est barré une seule fois
		 *2 si cost[i][j] est barré deux fois 
		 */
		int matrice_barre[n][n];
		initialize_matrix(n, matrice_barre);

		barrer(n, matrice_barre, lignes, colonnes);
		soustraction_addition(n, cost, matrice_barre);

	    }
	}while(!arret);

	affiche(n, assignement, cost_copie, argv[1], stdout);
	fichier = fopen("resultats.log", "a");
	if(feof(fichier))
	{
	    perror("Erreur lors de l'ouversture du fichier");
	    return EXIT_FAILURE;
	}
	affiche(n, assignement, cost_copie, argv[1], fichier);
	fclose(fichier);

	return 0;
    }
}	
