## Copyright 

    Copyright 2017 Abdellaziz Mohamed Amine

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

## Introduction

This is the source code of a short project I did during the completion of my master's degree in 
 Operations Research at the USTHB, Algiers. The goal was to  implement the Hungarian method in C.
 
Please visit to know more [my website](https://abdellazizamine.wordpress.com/projects/hungarian-method/).

## Compilation

To compile the program execute teh command:

`$ make`

and to delete the generated object files type:

`$ make clear` 

If you want to delete any generated file including the executable do:

`$ make clean`

## Usage

The folder *instances* contains as its name indicates instances of the Hunagrian method.

An example of execution:

`./hongrois instances/instance1_5x5.txt 5 5`
