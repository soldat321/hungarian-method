/*
Copyright 2017 Abdellaziz Mohamed Amine

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <locale.h>


void lecture(FILE *fichier,int n, int p, int q, double cost[n][n]);
void min_matrix(int n, double cost[n][n]);
void initialize_matrix(int n, int matrix[n][n]);
void initialize_vector(int n, int vector[n]);
void copy_matrix(int n, double matrix1[n][n], double matrix2[n][n]);
void affiche(int n, int matrix[n][n], double cost[n][n], char *instance, FILE *fichier);
void affiche_double(int n, double matrix[n][n], FILE *fichier);
int barre_carre(int n, int assignement[n][n], int intermediate[n][n]);
void cost_zeros(int n, double  cost[n][n],int intermediate[n][n]);
void marquer(int n, int intermediate[n][n], int lignes[n], int colonnes[n]);
void barrer(int n, int  matrice_barre[n][n], int lignes[n], int colonnes[n]);
void soustraction_addition(int n, double cost[n][n], int matrice_barre[n][n]);
